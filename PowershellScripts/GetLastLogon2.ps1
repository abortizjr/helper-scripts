# SETTINGS EMAIL REPORT 
$MailRecipient = @("armando@sdcsecurity.com","dwatters@sdcsecurity.com")
$MailSender = "No Reply <noreply@sdcsecurity.com>"
$MailServer = "sdcsecurity-com.mail.protection.outlook.com"
$MailSubject = "Monthly AD Last User Logon Report"
Get-ADUser -Filter * -Properties * | Where {$_.Enabled -eq $True} | Select-Object -Property Name,LastLogonDate | Export-csv c:/lastlogon.csv

#GENERATE EMAIL REPORT
foreach( $MailTo in $MailRecipient )
{
	Send-MailMessage -To "$MailTo" -From "$MailSender" -Subject "$MailSubject" -Body "See attached CSV flat file.  Import into spreadsheet.`n`nDo not reply to this message!" -SmtpServer "$MailServer" -Attachments c:/lastlogon.csv
}
exit 0