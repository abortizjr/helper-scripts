Function Get-Bios {
         Param($computer)
If(-not($computer))
{
         Write-Warning "Please supply computername"
}

If(Test-Connection -ComputerName $computer -BufferSize 16 -Count 1 -ea 0 -quiet)
{

Try
{
         $ErrorActionPreference ="stop"
Get-WmiObject -Class win32_bios -Computer $computer|
select @{n="ComputerName";e={$computer.toupper()}},SerialNumber,Manufacturer
}

Catch
{
         Write-Host "$computer `b" -NoNewline -BackgroundColor red
         Write-Warning $error[0]
}
}

Else
{
        Write-Host "$($computer) is offline" -ForegroundColor Red
}
}



If ( ! (Get-module ActiveDirectory )) {
Import-Module ActiveDirectory
Cls
}

get-ADComputer -filter * |select -exp name |foreach{Get-Bios -computer $_}