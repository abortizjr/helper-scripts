<?php
/*
        This is a simple parsing utility for retrieving and spewing out
        the values of certain Brother Printer metrics that get stored as
        hex values instead of simple values.

        You can retrieve toner levels this way:
        php getbrother.php <ip> <hex code for metric>

        Example:
                Black toner percentage
                php getbrother.php 192.168.100.106 81

        This script can be used for Zabbix, Nagios, or other system
        monitoring software.

        Special thanks to Maciej Bieniek for his Python script that helped
        me go in the right direction with this.

        https://github.com/bieniu/brother

        No thanks go to Brother Industries for making this more difficult
        than it needed to be.

        This script is provided without warranty of any kind.  You modify
        it, you own it.  You use it at your own risk.

        This requires the SNMP module for PHP to be installed.

        Armando Ortiz <abortizjr@gmail.com>

*/

error_reporting( 0 );

if( !filter_var( $argv[ 1 ], FILTER_VALIDATE_IP ) )
{
  die( "Invalid IP address.\n" );
}

if( strlen( $argv[ 2 ] ) != 2 )
{
        die( "Second argument must be a valid hex value.\n" );
}

// This returns a hex string that we have to parse from Brother printers - and it's totally asenine.
$thisOID = "1.3.6.1.4.1.2435.2.3.9.4.2.1.5.5.8.0";
$thisObject = snmp2_get( $argv[ 1 ], "public", $thisOID );

// Get rid of the Hex-STRING part by splitting it off.
$thisResult = explode( ":", str_replace( array( " ", "\n" ), "", $thisObject) );

$myHexVals = explode( "\n", chunk_split( $thisResult[ 1 ], 14 ) );

// Populate the array we'll be fetching our desired "key" from.
while( list( $key, $val ) = each( $myHexVals ) )
{
        $thisMetric = substr( $val, 0, 2 );
        $thisMetricVal = substr( $val, 6 );

        if( $thisMetric != "" )
                $theseVals[ $thisMetric ] =  hexdec( $thisMetricVal );
}

// Echo out our result - this is really all we need.
echo $theseVals[ $argv[ 2 ] ];

/*
        Constants grabbed from Maciej Bieniek's script:
    "11": VAL_DRUM_COUNT,
    "31": VAL_BLACK_TONER_STATUS,
    "32": VAL_CYAN_TONER_STATUS,
    "33": VAL_MAGENTA_TONER_STATUS,
    "34": VAL_YELLOW_TONER_STATUS,
    "41": VAL_DRUM_REMAIN,
    "63": VAL_DRUM_STATUS,
    "69": VAL_BELT_REMAIN,
    "6a": VAL_FUSER_REMAIN,
    "6b": VAL_LASER_REMAIN,
    "6c": VAL_PF_MP_REMAIN,
    "6d": VAL_PF_1_REMAIN,
    "6f": VAL_BLACK_TONER_REMAIN,
    "70": VAL_CYAN_TONER_REMAIN,
    "71": VAL_MAGENTA_TONER_REMAIN,
    "72": VAL_YELLOW_TONER_REMAIN,
    "73": VAL_CYAN_DRUM_COUNT,
    "74": VAL_MAGENTA_DRUM_COUNT,
    "75": VAL_YELLOW_DRUM_COUNT,
    "7e": VAL_BLACK_DRUM_COUNT,
    "79": VAL_CYAN_DRUM_REMAIN,
    "7a": VAL_MAGENTA_DRUM_REMAIN,
    "7b": VAL_YELLOW_DRUM_REMAIN,
    "80": VAL_BLACK_DRUM_REMAIN,
    "81": VAL_BLACK_TONER,
    "82": VAL_CYAN_TONER,
    "83": VAL_MAGENTA_TONER,
    "84": VAL_YELLOW_TONER,
    "a1": VAL_BLACK_TONER_REMAIN,
    "a2": VAL_CYAN_TONER_REMAIN,
    "a3": VAL_MAGENTA_TONER_REMAIN,
    "a4": VAL_YELLOW_TONER_REMAIN
*/
